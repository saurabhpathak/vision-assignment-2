//
//  main.cpp
//  vision1
//
//  Created by Saurabh Pathak on 23/10/2016.
//  Copyright © 2016 Trinity. All rights reserved.
//

#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;

int DELAY_BLUR = 100;
int MAX_KERNEL_LENGTH = 31;
int display_dst( int delay );

int main(int argc, const char * argv[]) {
    
    VideoCapture cap("Door1.avi");
    Mat image, canny_output, image_grey, cdst;
    double FPS = 24.0;
    while(true) {
        cap >> image;
        if (image.empty()) {
            std::cout << "Can't read the video";
            break;
        }
        Mat dst = image.clone();
        
        for ( int i = 1; i < MAX_KERNEL_LENGTH; i = i + 2 )
        { GaussianBlur( image, dst, Size( i, i ), 0, 0 ); }
        
        subtract(dst, image, dst);
        imshow("blurred", dst);
        
        cvtColor(image, image_grey, CV_BGR2GRAY);
        
        imshow("Video Reading", image);
        if (waitKey(1000.0/FPS) == 27) {
            break;
        }

        int thresh = 30;
        
        Canny( dst, canny_output, thresh, thresh*2, 3 );
        cvtColor(image_grey, cdst, COLOR_GRAY2BGR);
        imshow("Edge detected", canny_output);
        
//        vector<Vec4i> lines;
//        HoughLinesP(canny_output, lines, 1, CV_PI / 180, 50, 50, 10);
//        for (size_t i = 0; i < lines.size(); i++)
//        {
//            Vec4i l = lines[i];
//            Point pt1 = Point(l[0], l[1]);
//            Point pt2 = Point(l[2], l[3]);
//            float theta = atan2(pt2.y - pt1.y, pt2.x - pt1.x) * 180.0 / CV_PI;
//            if ((theta > 80 && theta < 100) || (theta > 0 && theta < 10)) {
//                line(cdst, pt1, pt2, Scalar(0, 0, 255), 3, 2);
//            }
//        }
        
        
        
        vector<Vec2f> lines;
        HoughLines(canny_output, lines, 1, CV_PI/180, 100, 0, 0 );
        for (size_t i = 0; i < lines.size(); i++)
        {
            float theta = lines[i][1], rho = lines[i][0];
            if((theta > (80*CV_PI/180) && theta < (100*CV_PI/180)) || (theta > 0 && theta < (10*CV_PI/180))) {
                Point pt1, pt2;
                double a = cos(theta), b = sin(theta);
                double x0 = a*rho, y0 = b*rho;
                pt1.x = cvRound(x0 + 1000*(-b)); //the first point
                pt1.y = cvRound(y0 + 1000*(a)); //the first point
                pt2.x = cvRound(x0 - 1000*(-b)); //the second point
                pt2.y = cvRound(y0 - 1000*(a)); //the second point
                line(cdst, pt1, pt2, Scalar(0, 0, 255), 3, 2);
            }
        }
        imshow("hough lines", cdst);
        
    }
    return 0;
}
